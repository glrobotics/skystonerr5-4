package org.firstinspires.ftc.teamcode.Team10785;

//import android.hardware.camera2.CameraDevice;

import com.qualcomm.robotcore.hardware.HardwareMap;
import java.util.Vector;
import com.vuforia.CameraDevice;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.JavaUtil;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaBase;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone;

import java.util.ArrayList;
import java.util.List;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.XYZ;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.BACK;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.FRONT;

import java.util.Collections;
import java.util.Comparator;


public class Vision {

    // Vision
    private VuforiaSkyStone vuforiaSkyStone = new VuforiaSkyStone();
    private TfodSkyStone tfodSkyStone = new TfodSkyStone();
    private VuforiaBase.TrackingResults vuforiaResults;

    // Since ImageTarget trackables use mm to specifiy their dimensions, we must use mm for all the physical dimension.
    // We will define some constants and conversions here
    private static final float mmPerInch        = 25.4f;

    private OpenGLMatrix lastLocation = null;
    private boolean targetVisible = false;
    private boolean bAlliance = false;

    public boolean skystonefound=false;
    public double runningavg=0;
    public int skystonenotfoundcount=0;
    public String stoneLoc ="";

    private int updatecount;


    public void initVuforia(HardwareMap hardwareMap,boolean blueAlliance){
        bAlliance=blueAlliance;
        // Sample TFOD Op Mode
        // Initialize Vuforia.
        vuforiaSkyStone.initialize(
                //*Sharp inhale*
                "AbQaekX/////AAABmdZK4VbDrU0cmz7SaHl/whRyPl7Ef/qgl6dy0r02zyIydswhIKnSJslloshmr7SR0dv9mi1bXIP2WrBUXMANqvWEVuEYCXUwPF2bxWiLRuzmmfJXPzusGPfVqJlYz5DPHoh+GXzErifqDn9pND1e8pxs5hCTdAwSAG4DeyMEhRXTuuLKKusNLKyDwoGjLR7ndnRoi5iQxFKRMnkpnY6XawJvQozMzIxP9NzKe3Sgyzr7Q+yh6AJyPOHEHz1Ftx3jvNb9U+n+l2rUlJxxlAwyAKf5/ugGo/T0BDtozghrwVDi6DTgNLzlbBIC4o8ly/UpF0/rPSt+hmMt0f+OA8yDe194IMDBf2VsAXethn52oh/e",
                //AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                hardwareMap.get(WebcamName.class, "Webcam 1"), // cameraName
                "", // webcamCalibrationFilename
                true, // useExtendedTracking
                true, // enableCameraMonitoring
                VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES, // cameraMonitorFeedback
                0, // dx
                0, // dy
                0, // dz
                0, // xAngle
                0, // yAngle
                0, // zAngle
                true); // useCompetitionFieldTargetLocations
        // Set min confidence threshold to 0.7

        //activates picture sensing
        vuforiaSkyStone.activate();

        //init skystone sensing
        tfodSkyStone.initialize(vuforiaSkyStone, 0.5F, true, true);
        tfodSkyStone.activate();
    }

    /**
     * needs to be run periodically to scan for stones and update the robot's position
     *
     * @return
     */
    public boolean update () {
        if (tfodSkyStone != null) {
            // getUpdatedRecognitions() will return null if no new information is available since
            // the last time that call was made.
            List<Recognition> updatedRecognitions = tfodSkyStone.getUpdatedRecognitions();
            if (updatedRecognitions != null) {
                skystonefound = false;
//                    telemetry.addData("# Object Detected", updatedRecognitions.size());
                // step through the list of recognitions and display boundary info.
                int i = 0;
                for (Recognition recognition : updatedRecognitions) {
                    if (recognition.getLabel().equalsIgnoreCase("Skystone")) {
                        //telemetry.addData(String.format("label (%d)", i), recognition.getLabel());
                        runningavg = (recognition.getLeft() * 0.2) + (runningavg * 0.8);
                        skystonefound = true;
                        skystonenotfoundcount = 0;
//                            telemetry.addData(String.format("  left,top (%d)", i), "%.03f , %.03f",
//                                    recognition.getLeft(), recognition.getTop());
                    }
                }
            }
        }
        if(bAlliance) {
            // Skystone Blue Alliance Logic
            if (!skystonefound) {
                skystonenotfoundcount++;
            }
            if (skystonenotfoundcount > 200) {//The Blue side is a little flakey, keep this at 200
                stoneLoc = "Load";//Right?
            } else if (runningavg > 300) {
                stoneLoc = "Center";//Duh
            } else {
                stoneLoc = "Build";//Left?
            }
            if (!skystonefound) {
                skystonenotfoundcount++;
            }
        }
        else {
            // SkyStone Red Alliance Logic
            if (skystonenotfoundcount > 100) {
                stoneLoc = "Build";
            } else if (runningavg > 421) {
                stoneLoc = "Build";//Right?
            } else if (runningavg < 255) {
                stoneLoc = "Load";//Left?
            } else {
                stoneLoc = "Center";//Duh
            }
        }


        // check all the trackable target to see which one (if any) is visible.

        targetVisible = false;
        if (isTargetVisible("Stone Target")) {
            processTarget();
        } else if (isTargetVisible("Blue Front Bridge")) {
            processTarget();
        } else if (isTargetVisible("Red Rear Bridge")) {
            processTarget();
        } else if (isTargetVisible("Red Front Bridge")) {
            processTarget();
        } else if (isTargetVisible("Red Rear Bridge")) {
            processTarget();
        } else if (isTargetVisible("Red Perimeter 1")) {
            processTarget();
        } else if (isTargetVisible("Red Perimeter 2")) {
            processTarget();
        } else if (isTargetVisible("Front Perimeter 1")) {
            processTarget();
        } else if (isTargetVisible("Front Perimeter 2")) {
            processTarget();
        } else if (isTargetVisible("Blue Perimeter 1")) {
            processTarget();
        } else if (isTargetVisible("Blue Perimeter 2")) {
            processTarget();
        } else if (isTargetVisible("Rear Perimeter 1")) {
            processTarget();
        } else if (isTargetVisible("Rear Perimeter 2")) {
            processTarget();

        }
        return targetVisible;
    }

    public float getRobotX (){
        if (lastLocation != null){
            VectorF translation = lastLocation.getTranslation();
            return(translation.get(0) / mmPerInch);
        } else
            return 0.0f;

    }

    public float getRobotY (){
        if (lastLocation != null){
            VectorF translation = lastLocation.getTranslation();
            return(translation.get(1) / mmPerInch);
        } else {
            return 12.0f;

        }
    }
    public float getRobotZ (){
        if (lastLocation != null){
            VectorF translation = lastLocation.getTranslation();
            return(translation.get(2) / mmPerInch);
        } else
            return 0.0f;
    }
    public float getRobotRoll(){
        if (lastLocation != null){
            Orientation rotation = Orientation.getOrientation(lastLocation, EXTRINSIC, XYZ, DEGREES);
            return(rotation.firstAngle);
        } else
            return 0.0f;
    }
    public float getRobotPitch(){
        if (lastLocation != null){
            Orientation rotation = Orientation.getOrientation(lastLocation, EXTRINSIC, XYZ, DEGREES);
            return(rotation.secondAngle);
        } else
            return 0.0f;
    }
    public float getRobotHeading(){
        if (lastLocation != null){
            Orientation rotation = Orientation.getOrientation(lastLocation, EXTRINSIC, XYZ, DEGREES);
            return(rotation.thirdAngle);
        }else
            return 0.0f;
    }

    /**
     * Check to see if the target is visible.
     */
    private boolean isTargetVisible(String trackableName) {
        boolean isVisible;

        // Get vuforia results for target.
        vuforiaResults = vuforiaSkyStone.track(trackableName);
        // Is this target visible?
        if (vuforiaResults.isVisible) {
            isVisible = true;
        } else {
            isVisible = false;
        }
        return isVisible;
    }

    /**
     * This function displays location on the field and rotation about the Z
     * axis on the field. It uses results from the isTargetVisible function.
     */
    private void processTarget() {
        // Display the target name.
        lastLocation = vuforiaResults.matrix;
    }

    /**
     * By default, distances are returned in millimeters by Vuforia.
     * Convert to other distance units (CM, M, IN, and FT).
     */
    private double displayValue(float originalValue, String units) {
        double convertedValue;

        // Vuforia returns distances in mm.
        if (units.equals("CM")) {
            convertedValue = originalValue / 10;
        } else if (units.equals("M")) {
            convertedValue = originalValue / 1000;
        } else if (units.equals("IN")) {
            convertedValue = originalValue / 25.4;
        } else if (units.equals("FT")) {
            convertedValue = (originalValue / 25.4) / 12;
        } else {
            convertedValue = originalValue;
        }
        return convertedValue;
    }

  public void shutdown (){
      vuforiaSkyStone.deactivate();
      tfodSkyStone.deactivate();
  }

}
