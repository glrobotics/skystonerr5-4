//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import java.security.KeyStore;
import java.util.TimerTask;

@Autonomous
public class SSAUTOResets extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public String found;
    public int target;
    public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float maxSpeed = .4f;
    public final int slidemax = 461;
    public final int liftmin = -570;
    public final float hookDown=1.0f;
    public final float hook2Down=0f;
    public final float hookUp=0.44f;
    public final float hook2Up=0.5f;
    public final float capstoneUp=0.1f;
    public final float capstoneDown=1.0f;
    public final float capstoneReleaseUp=0.5f;
    public final float capstoneReleaseDown=1;

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        robot.claw.setPosition(0);
        robot.startingAngle=0;
        robot.claw2.setPosition(1.2);
        robot.hook.setPosition(hookUp);
        robot.hook2.setPosition(hook2Up);
        robot.capstone.setPosition(capstoneUp);
        robot.capstoneRelease.setPosition(capstoneReleaseDown);
        robot.setHomePositions();

        waitForStart();

        while(opModeIsActive()) {

        }

        }
    }
