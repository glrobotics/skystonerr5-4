package org.firstinspires.ftc.teamcode.Team10785;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.robotcore.hardware.DcMotorController;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.MotorControlAlgorithm;
import com.qualcomm.robotcore.hardware.PIDCoefficients;
import com.qualcomm.robotcore.hardware.PIDFCoefficients;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.configuration.typecontainers.MotorConfigurationType;
import com.qualcomm.robotcore.util.ElapsedTime;
//import com.sun.tools.javac.tree.DCTree;
import com.qualcomm.hardware.bosch.BNO055IMU;
import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import java.util.Locale;

public class Robot10785 {
    //public CRServo collector2;
    //public CRServo extend2;
    //public CRServo extend1;
    //public DcMotor lift;
    //public DcMotor lift2;
    public Servo capstoneRelease;
    public Servo claw; //stacker part: Holds the blocks
    public Servo claw2;
    public Servo capstone;
    public Servo hook;
    public Servo hook2;
    public DcMotorEx lift; //stacker part: Raises the claw
    public DcMotorEx slide; //stacker part: Moves the lift out
    public DcMotorEx tape1;
    public DcMotorEx tape2;
    public double countsPerInchTape = 288/13.2;
    //public Servo teammarker;
    public DcMotorEx leftFrontMotor; //Wheel 1
    public DcMotorEx rightFrontMotor; //Wheel 2
    public DcMotorEx leftBackMotor; //Wheel 3
    public DcMotorEx rightBackMotor; //Wheel 4
    public static class MovePower{
        public float leftFront=0;
        public float leftBack=0;
        public float rightFront=0;
        public float rightBack=0;
        public void setAll(float MP){
            leftFront=leftBack=rightBack=rightFront=MP;
        }
        public void set(float fr,float fl, float bl, float br){
            leftFront=fl;
            leftBack=bl;
            rightBack=br;
            rightFront=fr;
        }
    }
    public MovePower movePower=new MovePower();

    public float moveTarget = 0;
    public float countPerInch = 45.0f;//WAS 172
    public float inchesPer360 = 79.0116f;
    public float initRobotAngle;
    public float cumulativeError;
    public float totalCumulative;
    public float correctionFactor;
    public PIDFCoefficients veloPIDF=new PIDFCoefficients();
    public PIDFCoefficients slidePID=new PIDFCoefficients();
    public PIDFCoefficients liftPIDF=new PIDFCoefficients();
    private FtcDashboard dashboard;
    private float accelTime;
    private float vMax;
    private float distance;
    private float startTime;
    private float t;
    private float t2;
    public float fieldX;
    public float fieldY;
    public float lastPos=0;
    public float currentPos=0;
    public float deltaPos=0;
    private float savedRobotAngle;
    private float deltaX=0;
    private float deltaY=0;
    private boolean strafeMoveComplete=false;
    private boolean moveMoveComplete=false;


    public final float capstoneUp=0.1f;
    public final float capstoneDown=0.55f;
    public final float capstoneReleaseUp=0.5f;
    public final float capstoneReleaseDown=1;

    public static BNO055IMU imu;
    public static double startingAngle=0;
    public double runningavg;

    public Orientation angles;
    //public Acceleration gravity;


    /* local OpMode members. */
    HardwareMap hwMap =  null;
    public ElapsedTime period = new ElapsedTime();


    public void Robot10785() {
    }

    //Chances are this is all you're here for:\\
    public double getVelocity(){
        return leftBackMotor.getVelocity();
        //Should we return the average velocity from all four?
    }
    public double getVelocityError(){
        return leftBackMotor.getVelocity()-(leftBackMotor.getPower()*3100);
        //Should we return the average velocity from all four?
    }

    public void turn() {
        moveStep = MoveStep.turn;
    }
    public void turn(float degrees) {
        moveTarget = degrees;
        moveStep = MoveStep.turn;
    }
    public void turn(float degrees,float v) {
        moveTarget = degrees;
        movePower.setAll(v);
        moveStep = MoveStep.turn;
    }
    public void moveStrafeFirst(float x,float y, float angle, float v){
        savedRobotAngle=angle*(float)(Math.PI/180.0f);
        deltaX=(x-fieldX);
        deltaY=(y-fieldY);
        strafeHoldAngle(deltaX*(float)Math.cos(savedRobotAngle),deltaY*(float)Math.sin(savedRobotAngle),v,angle);
        if(strafeMoveComplete){
            strafeMoveComplete=false;
            moveHoldAngle(deltaX*(float)Math.sin(savedRobotAngle),deltaY*(float)Math.cos(savedRobotAngle),v,angle);
        }
    }
    public void moveMoveFirst(float x,float y, float angle, float v){
        savedRobotAngle=angle*(float)(Math.PI/180.0f);
        deltaX=(x-fieldX);
        deltaY=(y-fieldY);
        moveHoldAngle(deltaX*(float)Math.sin(savedRobotAngle),deltaY*(float)Math.cos(savedRobotAngle),v,angle);
        if(moveMoveComplete){
            moveMoveComplete=false;
            strafeHoldAngle(deltaX*(float)Math.cos(savedRobotAngle),deltaY*(float)Math.sin(savedRobotAngle),v,angle);
        }
    }

    public void diagonalMoveRelative(float x, float y, float v, float angleToHold){
        initRobotAngle=angleToHold;
        deltaX=(x-fieldX);
        deltaY=(y-fieldY);
        moveTarget=(float)Math.sqrt((Math.pow(deltaX,2))+(Math.pow(deltaY,2)));
        float moveAngle=(float)Math.atan(x/y);
        movePower.set(-v*(float)Math.sin(moveAngle)-v*(float)Math.cos(moveAngle),-v*(float)Math.sin(moveAngle)-v*(float)Math.cos(moveAngle),
                      -v*(float)Math.cos(moveAngle)+v*(float)Math.sin(moveAngle),-v*(float)Math.cos(moveAngle)+v*(float)Math.sin(moveAngle));
        moveStep=MoveStep.diagonalStrafe;
    }


    public void moveHoldAngle(float x,float y,float v, float angle){
        if(x>y) moveTarget=x;
        if(y>x) moveTarget=y;
        movePower.setAll(v);
        initRobotAngle=angle;
        moveStep=MoveStep.moveHoldAngle;
    }

    public void moveHoldAngle(float d,float v, float angle1){
        initRobotAngle=angle1;
        if ((getRobotAngle() > 315) || (getRobotAngle() < 45)) moveTarget = d-fieldY;
        else if ((getRobotAngle() > 135) && (getRobotAngle() < 225)) moveTarget = fieldY -d;
        else if ((getRobotAngle() > 45) && (getRobotAngle() < 135)) moveTarget = d - fieldX;
        else moveTarget = fieldX - d;
        movePower.setAll(v);
        moveStep=MoveStep.moveHoldAngle;
    }

    public void strafeHoldAngle(float x,float y,float v,float angle){
        if(x>y) moveTarget=x;
        if(y>x) moveTarget=y;
        movePower.setAll(v);
        initRobotAngle=angle;
        moveStep = moveStep.strafeHoldAngle;
    }

    public void strafeHoldAngle(float d, float v, float angletohold) {
        movePower.setAll(v);
        if ((getRobotAngle() > 315) || (getRobotAngle() < 45)) moveTarget = fieldX - d;
        else if ((getRobotAngle() > 135) && (getRobotAngle() < 225)) moveTarget = d - fieldX;
        else if ((getRobotAngle() > 45) && (getRobotAngle() < 135)) moveTarget = d - fieldY;
        else moveTarget = fieldY - d;
        moveStep = moveStep.strafeHoldAngle;
        initRobotAngle = angletohold;
    }


    public void relativeMove(float d,float v, float angle1){
        initRobotAngle=angle1;
        moveTarget = d;
        movePower.setAll(v);

        moveStep=MoveStep.moveHoldAngle;
    }

    public void relativeStrafe(float d, float v, float angletohold) {
        movePower.setAll(v);

        moveTarget = d;
        moveStep = moveStep.strafeHoldAngle;
        initRobotAngle = angletohold;
    }

    public void turnOnPoint(float angle,float v){
        moveTarget=angle;
        movePower.setAll(v);

        moveStep=MoveStep.turnOnPoint;
    }
    public boolean moveComplete(){
        if(moveStep==MoveStep.endEverything){
            return true;
        }
        else return false;
    }
    //end of all you're here for

    public void setrobotradius(double rad){
        inchesPer360 = (float)(rad * 2 * Math.PI);
    }
    public enum MoveStep{
        strafe,move,endStrafe,turn, endTurn1,endTurn2, endMove, turnOnPoint,strafeHoldAngle,moveHoldAngle,moveHoldAngle2,move2,move3,endEverything,
        strafeAngleCorrection,moveProfile,moveProfileStrafe,diagonalStrafe,diagonalStrafeAngleCorrection,endDiagonalStrafe
    }
    MoveStep moveStep=MoveStep.strafe;
    public void init(HardwareMap ahwMap) {

        hwMap = ahwMap;

        //EVERY SERVO OR MOTOR MUST HAVE AN ENTRY HERE:
        {
            //Expansion hub 1
            {
                lift = hwMap.get(DcMotorEx.class, "lift"); //motor 0
                slide = hwMap.get(DcMotorEx.class, "slide"); //motor 1
                tape1 = hwMap.get(DcMotorEx.class, "tape1");
                tape2 = hwMap.get(DcMotorEx.class, "tape2");
                claw2=hwMap.get(Servo.class,"claw2");
                claw = hwMap.get(Servo.class, "claw"); //servo
                hook = hwMap.get(Servo.class, "hook"); // hook for foundation
                hook2 = hwMap.get(Servo.class, "hook2"); // hook2 for foundation
                capstone=hwMap.get(Servo.class,"capstone");
                capstoneRelease = hwMap.get(Servo.class, "capstoneRelease");
            }//ends expansion hub 1

            //expansion hub 2
            {
                //teammarker = hwMap.get(Servo.class, "teammarker"); //
                leftFrontMotor = hwMap.get(DcMotorEx.class, "lfWheel");
                leftBackMotor = hwMap.get(DcMotorEx.class, "lrWheel");
                rightFrontMotor = hwMap.get(DcMotorEx.class, "rfWheel");
                rightBackMotor = hwMap.get(DcMotorEx.class, "rrWheel");
            }//ends expansion hub 2
        }//ends outer grouping
        lift.setDirection(DcMotor.Direction.FORWARD);
        lift.setTargetPosition(lift.getCurrentPosition());
        lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        lift.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        slide.setDirection(DcMotor.Direction.REVERSE);
        slide.setTargetPosition(slide.getCurrentPosition());
        slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        slide.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        tape1.setDirection(DcMotor.Direction.REVERSE);
        tape2.setDirection(DcMotor.Direction.FORWARD);
        tape1.setTargetPosition(slide.getCurrentPosition());
        tape1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        tape1.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        tape2.setTargetPosition(slide.getCurrentPosition());
        tape2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        tape2.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);


        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);
        leftBackMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.FORWARD);
        rightBackMotor.setDirection(DcMotor.Direction.FORWARD);

        // Must Stop and Reset before using Run With Encoder
        leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);


        leftFrontMotor.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        leftBackMotor.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        rightFrontMotor.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);
        rightBackMotor.setZeroPowerBehavior(DcMotorEx.ZeroPowerBehavior.BRAKE);

        veloPIDF.p=5;//WAS 10
        veloPIDF.d=0;
        veloPIDF.i=3;//WAS 6
        veloPIDF.f=2;
        veloPIDF.algorithm=MotorControlAlgorithm.PIDF;
        leftFrontMotor.setPIDFCoefficients(DcMotorEx.RunMode.RUN_USING_ENCODER,veloPIDF);
        leftBackMotor.setPIDFCoefficients(DcMotorEx.RunMode.RUN_USING_ENCODER,veloPIDF);
        rightBackMotor.setPIDFCoefficients(DcMotorEx.RunMode.RUN_USING_ENCODER,veloPIDF);
        rightFrontMotor.setPIDFCoefficients(DcMotorEx.RunMode.RUN_USING_ENCODER,veloPIDF);

        slidePID.p=15;//was 5
        slidePID.i=0;
        slidePID.d=0;
        slidePID.f=0;
        slidePID.algorithm=MotorControlAlgorithm.PIDF;
        slide.setPIDFCoefficients(DcMotorEx.RunMode.RUN_TO_POSITION,slidePID);

        liftPIDF.p=10;//was 5
        liftPIDF.i=0;
        liftPIDF.d=0;
        liftPIDF.f=0;
        liftPIDF.algorithm=MotorControlAlgorithm.PIDF;
        lift.setPIDFCoefficients(DcMotorEx.RunMode.RUN_TO_POSITION,slidePID);

        dashboard = FtcDashboard.getInstance();
        dashboard.setTelemetryTransmissionInterval(25);



    }

    public void updateFieldCoords(){
        lastPos=currentPos;
        currentPos=(rightFrontMotor.getCurrentPosition()+rightBackMotor.getCurrentPosition()+leftBackMotor.getCurrentPosition()+leftFrontMotor.getCurrentPosition())/(4*countPerInch);
        deltaPos=currentPos-lastPos;
        savedRobotAngle=getRobotAngleRad();
        fieldX-=deltaPos*Math.sin(savedRobotAngle);
        fieldY+=deltaPos*Math.cos(savedRobotAngle);
    }

    public void setWheelSpeeds(double LF, double LB, double RF, double RB){
        this.leftFrontMotor.setPower(LF);
        this.leftFrontMotor.setVelocity(LF*3100);

        this.rightFrontMotor.setPower(RF);
        this.rightFrontMotor.setVelocity(RF*3100);

        this.leftBackMotor.setPower(LB);
        this.leftBackMotor.setVelocity(LB*3100);

        this.rightBackMotor.setPower(RB);
        this.rightBackMotor.setVelocity(RB*3100);

        TelemetryPacket packet = new TelemetryPacket();
        packet.put("Power Left Front: ", LF);
        packet.put("Power Right Front : ", RF);
        packet.put("Power Left Rear: ", LB);
        packet.put("Power Right Rear: ", RB);

       // dashboard.sendTelemetryPacket(packet);
    }

    public void setHomePositions(){
        lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);


        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit            = BNO055IMU.AngleUnit.DEGREES;
        parameters.accelUnit            = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        parameters.loggingEnabled       = true;
        parameters.useExternalCrystal   = true;
        parameters.mode                 = BNO055IMU.SensorMode.IMU;
        parameters.loggingTag           = "IMU";
        imu                             = hwMap.get(BNO055IMU.class, "imu");

        imu.initialize(parameters);

    }



    public float getRobotAngle() {
        angles  = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.DEGREES);
        if (angles.firstAngle<0) angles.firstAngle=angles.firstAngle*(-1.0f);
        else angles.firstAngle=-angles.firstAngle+360.0f;
        return (angles.firstAngle);
    }
    public float getRobotAngleRad() {
        angles  = imu.getAngularOrientation(AxesReference.INTRINSIC, AxesOrder.ZYX, AngleUnit.RADIANS);
        return (angles.firstAngle);
    }



    public void moveUpdate () {
        switch (moveStep) {
            case endEverything:
                break; //End everything

                //MOVE THE ROBOT
            case move:
                leftFrontMotor.setPower(movePower.leftFront);
                leftBackMotor.setPower(movePower.leftBack);
                rightFrontMotor.setPower(movePower.rightFront);
                rightBackMotor.setPower(movePower.rightBack);
                moveStep = MoveStep.move2; //this and the break statement can be removed to no effect
                break;
            case moveHoldAngle:
                leftFrontMotor.setPower(movePower.leftFront);
                leftBackMotor.setPower(movePower.leftBack);
                rightFrontMotor.setPower(movePower.rightFront);
                rightBackMotor.setPower(movePower.rightBack);
                lastPos=0;
                currentPos=0;
                deltaPos=0;
                moveStep = MoveStep.moveHoldAngle2;
                break;
            case moveHoldAngle2:

                correctionFactor=-0.012f*((((getRobotAngle()-initRobotAngle)+180)%360)-180);
                cumulativeError+=(0.009f)*correctionFactor;
                totalCumulative=cumulativeError+correctionFactor;
                leftFrontMotor.setPower(movePower.leftFront+totalCumulative);
                leftBackMotor.setPower(movePower.leftBack+totalCumulative);
                rightFrontMotor.setPower(movePower.rightFront-totalCumulative);
                rightBackMotor.setPower(movePower.rightBack-totalCumulative);
                updateFieldCoords();

                if ((Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(leftBackMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightBackMotor.getCurrentPosition()) <= 0))
                    moveStep=MoveStep.endMove;
                break;

            case endMove:
                if (Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);

                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveMoveComplete=true;
                    moveStep = MoveStep.endEverything;
                }
                break;
                //END OF MOVING THE ROBOT
            case diagonalStrafe:
                leftFrontMotor.setPower(movePower.leftFront);
                leftBackMotor.setPower(movePower.leftBack);
                rightFrontMotor.setPower(movePower.rightFront);
                rightBackMotor.setPower(movePower.rightBack);
                moveStep = MoveStep.diagonalStrafeAngleCorrection;
                break;
            case diagonalStrafeAngleCorrection:
                correctionFactor=-0.012f*((((getRobotAngle()-initRobotAngle)+180)%360)-180);
                cumulativeError+=(0.009f)*correctionFactor;
                totalCumulative=cumulativeError+correctionFactor;
                leftFrontMotor.setPower(movePower.leftFront+totalCumulative);
                leftBackMotor.setPower(movePower.leftBack+totalCumulative);
                rightFrontMotor.setPower(movePower.rightFront-totalCumulative);
                rightBackMotor.setPower(movePower.rightBack-totalCumulative);
                updateFieldCoords();

                if ((Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(leftBackMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightBackMotor.getCurrentPosition()) <= 0))
                    moveStep=MoveStep.endDiagonalStrafe;
                break;

            case endDiagonalStrafe:
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);

                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = MoveStep.endEverything;

                break;

            //STRAFE
            case strafe:
			  leftFrontMotor.setPower(-movePower.leftFront);
                leftBackMotor.setPower(movePower.leftBack);
                rightFrontMotor.setPower(-movePower.rightFront);
                rightBackMotor.setPower(movePower.rightBack);
                moveStep = MoveStep.endStrafe;
                break;
            //END OF STRAFE
           
           case strafeHoldAngle://STRAFE WITH ANGLE CORRECTION
             cumulativeError=0;
                leftFrontMotor.setPower(-movePower.leftFront);
                leftBackMotor.setPower(movePower.leftBack);
                rightFrontMotor.setPower(-movePower.rightFront);
                rightBackMotor.setPower(movePower.rightBack);
                lastPos=0;
                currentPos=0;
                deltaPos=0;
                moveStep = MoveStep.strafeAngleCorrection;
                break;
            case strafeAngleCorrection://STRAFE ANGLE CORRECTION CALCULATIONS
                correctionFactor=-0.024f*((((getRobotAngle()-initRobotAngle)+180)%360)-180);
                cumulativeError+=(0.009f)*correctionFactor;
                totalCumulative=cumulativeError+correctionFactor;
                leftFrontMotor.setPower(-movePower.leftFront+totalCumulative);
                leftBackMotor.setPower(movePower.leftBack+totalCumulative);
                rightFrontMotor.setPower(-movePower.rightFront-totalCumulative);
                rightBackMotor.setPower(movePower.rightBack-totalCumulative);
                updateFieldCoords();
                if ((Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(leftBackMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightBackMotor.getCurrentPosition()) <= 0))
                    moveStep=MoveStep.endStrafe;
                break;

            case endStrafe://CHEKCS IF MOVETARGET IS ACQUIRED AND STOPS ROBOT
                if ((Math.abs(moveTarget*countPerInch) - Math.abs(leftFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightFrontMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(leftBackMotor.getCurrentPosition()) <= 0)
                        &&(Math.abs(moveTarget*countPerInch) - Math.abs(rightBackMotor.getCurrentPosition()) <= 0)) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);


                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    strafeMoveComplete=true;
                    moveStep = MoveStep.endEverything;

                }
                break;

            case turn: //TURNS THE ROBOT
                if(((((getRobotAngle()-moveTarget)+180)%360)-180)< 0){
                    leftFrontMotor.setPower(movePower.leftFront);
                    leftBackMotor.setPower(movePower.leftBack);
                    rightFrontMotor.setPower(-movePower.rightFront);
                    rightBackMotor.setPower(-movePower.rightBack);
                    moveStep = MoveStep.endTurn1;
                }
                else{
                    leftFrontMotor.setPower(-movePower.leftFront);
                    leftBackMotor.setPower(-movePower.leftBack);
                    rightFrontMotor.setPower(movePower.rightFront);
                    rightBackMotor.setPower(movePower.rightBack);
                    moveStep = MoveStep.endTurn2;
                }
                break;

            case endTurn1://ENDS TURN
                if (((((getRobotAngle()-moveTarget)+180)%360)-180)>= -5) {

                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = MoveStep.endEverything;
                }
                break;

            case endTurn2://ENDS TURN OTHER DIRECTION
                if (0-((((getRobotAngle()-moveTarget)+180)%360)-180)>= 5) {
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
                    leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    leftBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    rightBackMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
                    moveStep = MoveStep.endEverything;
                }
                break;
            case turnOnPoint://TURN ON POINT (TURNS ABOUT RIGHT BACK WHEEL)
                if(moveTarget-getRobotAngle() < 0){
                    leftFrontMotor.setPower(movePower.leftFront);
                    leftBackMotor.setPower(movePower.leftBack);
                    rightFrontMotor.setPower(0);
                    rightBackMotor.setPower(0);
                    moveStep = MoveStep.endTurn1;
                }
                else{
                    leftFrontMotor.setPower(0);
                    leftBackMotor.setPower(0);
                    rightFrontMotor.setPower(movePower.rightFront);
                    rightBackMotor.setPower(movePower.rightBack);
                    moveStep = MoveStep.endTurn2;
                }
                break;



            default:
                moveStep = MoveStep.endEverything;
                break;
        }
}

}
