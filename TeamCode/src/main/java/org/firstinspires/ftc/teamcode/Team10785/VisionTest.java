package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp

public class VisionTest extends LinearOpMode {


    public Vision visionSystem;


    @Override
    public void runOpMode(){

        visionSystem = new Vision();
        visionSystem.initVuforia(hardwareMap,true);

        telemetry.addData("Stauts","Initialized");
        telemetry.update();

        waitForStart();





        while (opModeIsActive()){
            visionSystem.update();

            telemetry.addData("Status","Running");
            telemetry.addData("Found","%s",visionSystem.stoneLoc);
            telemetry.addData("Running Avg",visionSystem.runningavg);
            telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
                    visionSystem.getRobotX(), visionSystem.getRobotY(), visionSystem.getRobotZ());


            telemetry.update();
        }

       visionSystem.shutdown();

    }
}
