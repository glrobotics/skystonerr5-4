//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DcMotor;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone;

import org.firstinspires.ftc.robotcore.external.JavaUtil;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaBase;


import java.util.ArrayList;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.BACK;

@Autonomous
public class ThisCanBeUsedForTesting extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public String found;
    public int target;
    //public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float superFast=0.9f;
    public float maxSpeed = 0.65f;//was 0.4
    public float slowSpeed=0.4f;
    public float accTime = 1.0f;
    private float reverseDistance = 0;
    public final int liftceil = 2226;
    public final int liftfloor = 119;
    public final int slidemax = 461;
    public final int slideOutBlock=205;
    public final int liftmin = -570;

    private final float k = -1.0f;//A constant made for ease of transferral
    public final float clawOpen=1.2f;
    public final float claw2Open=0f;
    public final float claw2Close=1.05f;
    public final float clawClose=0.6f;
    public final float hookDown=1.0f;
    public final float hook2Down=0f;
    public final float hookUp=0.44f;
    public final float hook2Up=0.5f;
    public int[] liftClearHeight={liftfloor,448,548,869,1115,1361,1610,1944};
    public int[] liftPlaceHeight={liftfloor,148,405,691,1034,1287,1490,1703};
    public int clearCount=0;


    //Back Left of robot compared to corner of building zone

    public enum AutonomousStep{
        one,two,three,four,five,six,endAuto,end

    }
    public AutonomousStep autonomousStep = AutonomousStep.one;



    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        robot.setHomePositions();
        robot.startingAngle = 0;
        robot.hook.setPosition(hookUp);
        robot.hook2.setPosition(hook2Up);

        telemetry.addData("Status", "Initialized 1");
        telemetry.update();


//        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setTargetPosition(0);
        robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.lift.setPower(1);

//        robot.slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.slide.setTargetPosition(0);
        robot.slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slide.setPower(1);

        robot.tape1.setTargetPosition(0);
        robot.tape2.setTargetPosition(0);
        robot.tape1.setPower(1);
        robot.tape2.setPower(1);
        robot.fieldY=24.0f;
        robot.fieldX=24.0f;
        robot.claw.setPosition(0);
        robot.claw2.setPosition(claw2Close);

        robot.capstone.setPosition(robot.capstoneUp);
        robot.capstoneRelease.setPosition(robot.capstoneReleaseDown);


        // Sample TFOD Op Mode
        // Initialize Vuforia.

        robot.fieldX = 96;
        robot.fieldY = 0;
        while (!opModeIsActive() && !isStopRequested()) {

            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.addData("Robot X, Y:  ", "%f, %f", robot.fieldX, robot.fieldY);
            telemetry.update();

        }

        //waitForStart();


        while (opModeIsActive()) {


            switch (autonomousStep) {
                case one:
                    robot.setWheelSpeeds(-maxSpeed,-maxSpeed,maxSpeed,maxSpeed);
                    robot.moveTarget=180;
                    autonomousStep=AutonomousStep.endAuto;
                    break;
                case endAuto:
                    if (robot.moveComplete()){
                        autonomousStep = AutonomousStep.end;
                    }
                    break;

            }


            robot.moveUpdate();
            telemetry.addData("Status", "Running");
            telemetry.addData("Robot X, Y:  ", "%f, %f", robot.fieldX, robot.fieldY);
            telemetry.addData("MoveStep: ", "%s", robot.moveStep);
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("AutonomousStep", "%s", autonomousStep.name());
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            telemetry.addData("armPosition", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.update();

        }
    }
}
