//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DcMotor;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone;

import org.firstinspires.ftc.robotcore.external.JavaUtil;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaBase;


import java.util.ArrayList;

import static org.firstinspires.ftc.robotcore.external.navigation.AngleUnit.DEGREES;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesOrder.YZX;
import static org.firstinspires.ftc.robotcore.external.navigation.AxesReference.EXTRINSIC;
import static org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer.CameraDirection.BACK;

@Autonomous
public class SSAUTO1BlockBlueONEBLOCK extends LinearOpMode {

    Robot10785 robot = new Robot10785();
    public Vision visionSystem;

    public String found;
    public int target;
    //public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float superFast=0.9f;
    public float maxSpeed = 0.65f;//was 0.4
    public float slowSpeed=0.4f;
    public float accTime = 1.0f;
    private float reverseDistance = 0;
    public final int liftceil = 2226;
    public final int liftfloor = 119;
    public final int slidemax = 461;
    public final int slideOutBlock=205;
    public final int liftmin = -570;

    private final float k = -1.0f;//A constant made for ease of transferral
    public final float clawOpen=1.2f;
    public final float claw2Open=0f;
    public final float claw2Close=1.05f;
    public final float clawClose=0.6f;
    public final float hookDown=1.0f;
    public final float hook2Down=0f;
    public final float hookUp=0.44f;
    public final float hook2Up=0.5f;
    public int[] liftClearHeight={liftfloor,448,548,869,1115,1361,1610,1944};
    public int[] liftPlaceHeight={liftfloor,148,405,691,1034,1287,1490,1703};
    public int clearCount=0;


    //Back Left of robot compared to corner of building zone

    public enum AutonomousStep{
        moveFromHomePosition, strafeToFirstBlock, moveForwardToFirstBlock, lowerLift, grabBlock1, bringLiftUp1,
        moveBack1, turnToDrive1, moveUnderBar1, turnTowardsTray1, moveIntoTray1, releaseBlock1, moveAwayFromTray1,
        turnTowardsLoad1, moveBackToBlock2, turnToPickUpBlock2,moveForwardAndKnockBlock, moveToBlock2, sideWaysToLoadBlock,
        lowerLift2,grabBlock2, moveBack2, turnToTray2, moveToTray2, turnTowardsTray2, moveTowardsTray2,dropBlock2,moveToCenterTray,
        hookTray, moveToBuildZone, endAuto,turnToBuildZone,turnToBuildZone2,turnToBuildZone3,backToBuildZone,hookTrayPart2, liftHooks,
        strafeToPicture, turnToBlock2, turnToDrive2, moveUnderBar2, moveToPark, turnToPark, end

    }
    public AutonomousStep autonomousStep = AutonomousStep.moveFromHomePosition;



    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        robot.setHomePositions();
        robot.startingAngle = 0;
        robot.hook.setPosition(hookUp);
        robot.hook2.setPosition(hook2Up);

        telemetry.addData("Status", "Initialized 1");
        telemetry.update();


//        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setTargetPosition(0);
        robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.lift.setPower(1);

//        robot.slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.slide.setTargetPosition(0);
        robot.slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slide.setPower(1);

        robot.tape1.setTargetPosition(0);
        robot.tape2.setTargetPosition(0);
        robot.tape1.setPower(1);
        robot.tape2.setPower(1);

        robot.claw.setPosition(0);
        robot.claw2.setPosition(claw2Close);

        robot.capstone.setPosition(robot.capstoneUp);
        robot.capstoneRelease.setPosition(robot.capstoneReleaseDown);


        // Sample TFOD Op Mode
        // Initialize Vuforia.
        visionSystem = new Vision();
        visionSystem.initVuforia(hardwareMap, true);
        robot.fieldX = 96;
        robot.fieldY = 0;
        while (!opModeIsActive() && !isStopRequested()) {

            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.addData("Robot X, Y:  ", "%f, %f", robot.fieldX, robot.fieldY);
            telemetry.addData("Vision: ", "%f, %f", visionSystem.getRobotX(), visionSystem.getRobotY());
            telemetry.addData("Skystone location: ", visionSystem.stoneLoc);
            telemetry.addData("  Running avg: ", "%f", visionSystem.runningavg);
            telemetry.addData("Skystone not found", "%d", visionSystem.skystonenotfoundcount);
            telemetry.update();

            visionSystem.update();
        }
        //waitForStart();


        while (opModeIsActive()) {


            switch (autonomousStep) {
                case moveFromHomePosition:// LIFT LEVEL 1/Open claw/Move forward
                    robot.lift.setTargetPosition(liftClearHeight[0]);
                    robot.claw.setPosition(clawOpen);
                    robot.claw2.setPosition(claw2Open);
                    robot.diagonalMoveRelative(90.0f,40f, slowSpeed * k, 0);
                    autonomousStep = AutonomousStep.endAuto;
                    break;


                case moveForwardToFirstBlock://MOVES FORWARD TO 1ST BLOCK
                    if (robot.moveComplete()) {
                        if (visionSystem.stoneLoc.equals("Build")) {
                            robot.diagonalMoveRelative(90.0f,25f, -slowSpeed * k, 0);
                        } else if (visionSystem.stoneLoc.equals("Center")) {
                            robot.diagonalMoveRelative(94.5f,25f, -slowSpeed * k, 0);
                        } else {
                            robot.diagonalMoveRelative(106.5f,25f, slowSpeed * k, 0);
                        }
                        autonomousStep = AutonomousStep.lowerLift;
                    }
                    break;

                case lowerLift:
                    if (robot.moveComplete()) {
                        robot.lift.setTargetPosition(liftClearHeight[0]);
                        autonomousStep = AutonomousStep.grabBlock1;
                    }
                    break;

                case grabBlock1:
                    if (robot.lift.getCurrentPosition() < 140) {
                        robot.claw.setPosition(clawClose);
                        robot.claw2.setPosition(claw2Close);
                        robot.slide.setTargetPosition(slideOutBlock);//max is 127
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.bringLiftUp1;
                    }
                    break;


                case bringLiftUp1://BRINGS LIFT UP TO 60
                    if (robot.period.seconds() - startTime > 0.5) {
                        robot.lift.setTargetPosition(133);
                        autonomousStep = AutonomousStep.moveBack1;
                    }
                    break;

                case moveBack1: // MOVES BACKWARDS AFTER GRABBING BLOCK
                    if (Math.abs(robot.lift.getCurrentPosition() - 133) < 10) {
                        robot.moveHoldAngle(24.0f, -maxSpeed, 0); // was 12
                        autonomousStep = AutonomousStep.turnToDrive1;
                    }
                    break;

                case turnToDrive1:
                    if (robot.moveComplete()) {
                        robot.turn(-80, slowSpeed);
                        autonomousStep = AutonomousStep.moveUnderBar1;
                    }
                    break;

                case moveUnderBar1:
                    if (robot.moveComplete()) {
                        startTime = robot.period.seconds();
                        if (visionSystem.stoneLoc.equals("Build")) {
                            robot.moveHoldAngle(34.0f, superFast, 270);
                        } else if (visionSystem.stoneLoc.equals("Center")) {
                            robot.moveHoldAngle(38.0f, superFast, 270);
                        } else {
                            robot.moveHoldAngle(16.5f, superFast, 270);
                        }
                        autonomousStep = AutonomousStep.turnTowardsTray1;
                    }
                    break;
                case turnTowardsTray1:
                    if (robot.period.seconds() - startTime >= 0.5) {
                        robot.lift.setTargetPosition(liftClearHeight[1]);
                        robot.slide.setTargetPosition(slideOutBlock);
                    }
                    if (robot.moveComplete()) {
                        robot.turn(355, slowSpeed);
                        autonomousStep = AutonomousStep.moveIntoTray1;
                    }
                    break;

                case moveIntoTray1: // LIFT UP AND MOVE SLIDE OUT AND MOVE FORWARD 10 INCHES

                    if (robot.moveComplete() && (robot.lift.getCurrentPosition() - 448 <= 5)) {
                        robot.moveHoldAngle(27.5f, slowSpeed, 0);
                        autonomousStep = AutonomousStep.releaseBlock1;
                    }
                    break;

                case releaseBlock1:// OPEN CLAW/RELEASE BLOCK
                    if (robot.moveComplete()) {
                        robot.claw.setPosition(clawOpen);
                        robot.claw2.setPosition(claw2Open);
                        robot.lift.setTargetPosition(liftClearHeight[2]);
                        robot.hook.setPosition(hookDown);
                        robot.hook2.setPosition(hook2Down);
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.hookTray;
                    }
                    break;

                case hookTray: //MOVE FORWARD 4 INCHES AND SET LIFT POSITION TO 0 AND EXTEND TAPE AND MOVE SLIDE IN
                    if (robot.period.seconds() - startTime > .5) {
                        robot.moveHoldAngle(31f, slowSpeed, 0);
                        robot.claw.setPosition(clawClose);
                        robot.claw2.setPosition(claw2Close);
                        autonomousStep = AutonomousStep.backToBuildZone;
                    }
                    break;

                case backToBuildZone: //MOVE BACK
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(5, -maxSpeed, -15);
                        autonomousStep = AutonomousStep.turnToBuildZone;
                    }
                    break;

                //ADD THREE SEPERATE STEPS, 5 DEGREES, BACK, 85 DEGREES
                case turnToBuildZone:
                    if (robot.moveComplete()) {
                        robot.turn(-90, slowSpeed);
                        autonomousStep = AutonomousStep.moveToBuildZone;
                    }
                    break;
                case moveToBuildZone:
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(25, slowSpeed, 270);
                        autonomousStep = AutonomousStep.liftHooks;
                    }
                    break;

                case liftHooks:
                    if (robot.moveComplete()) {
                        robot.hook.setPosition(hookUp);
                        robot.hook2.setPosition(hook2Up);
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.strafeToPicture;
                    }
                    break;

                case strafeToPicture:
                    if (robot.period.seconds() - startTime > .5) {
                        robot.relativeStrafe(-9, -maxSpeed, 270);
                        autonomousStep = AutonomousStep.moveBackToBlock2;
                    }
                    break;

                case moveBackToBlock2:
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(104, -superFast, 270);
                        robot.lift.setTargetPosition(0);
                        robot.claw.setPosition(clawOpen);
                        robot.claw2.setPosition(claw2Open);
                        autonomousStep = AutonomousStep.turnToBlock2;
                    }
                    break;

                case turnToBlock2:
                    if (robot.moveComplete()) {
                        robot.turn(350, slowSpeed);
                        autonomousStep = AutonomousStep.moveToBlock2;
                    }
                    break;

                case moveToBlock2:
                    if (robot.moveComplete()) {
                        robot.relativeMove(2, maxSpeed, 0);
                        autonomousStep = AutonomousStep.grabBlock2;
                    }
                    break;

                case grabBlock2:
                     if (robot.moveComplete()){
                         robot.claw.setPosition(clawClose);
                         robot.claw2.setPosition(claw2Close);
                         startTime = robot.period.seconds();
                         autonomousStep = AutonomousStep.moveBack2;
                     }
                    break;
                case moveBack2:
                    if (robot.period.seconds()-startTime > .5){
                        robot.lift.setTargetPosition(133);
                        robot.relativeMove(-2, -maxSpeed, 0);
                        autonomousStep = AutonomousStep.turnToDrive2;
                    }
                    break;

                case turnToDrive2:
                    if (robot.moveComplete()){
                        robot.turn(-80, slowSpeed);
                        autonomousStep = AutonomousStep.moveUnderBar2;
                    }
                    break;

                case moveUnderBar2:
                    if (robot.moveComplete()){
                        robot.relativeMove(87, superFast, 270);
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.dropBlock2;
                    }
                    break;


                case dropBlock2:
                    if (robot.period.seconds() - startTime > 1){
                        robot.lift.setTargetPosition(liftClearHeight[2]);
                        robot.slide.setTargetPosition(slideOutBlock);
                    }
                    if (robot.moveComplete()){
                        robot.claw.setPosition(clawOpen);
                        robot.claw2.setPosition(claw2Open);
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.moveToPark;
                    }
                    break;

                case moveToPark:
                    if (robot.period.seconds() - startTime >.5){
                        robot.relativeMove(-2, -slowSpeed, 270);
                        autonomousStep = AutonomousStep.turnToPark;
                    }
                    break;

                case turnToPark:
                    if (robot.moveComplete()){
                        robot.turn(195, slowSpeed);
                        robot.tape1.setTargetPosition((int)(72 * robot.countsPerInchTape));
                        autonomousStep = AutonomousStep.endAuto;
                    }
                    break;

                case endAuto:
                    if (robot.moveComplete()){
                        autonomousStep = AutonomousStep.end;
                    }
            }


            robot.moveUpdate();
            visionSystem.update();
            telemetry.addData("Status", "Running");
            telemetry.addData("Robot X, Y:  ", "%f, %f", robot.fieldX, robot.fieldY);
            telemetry.addData("Vision: ", "%f, %f", visionSystem.getRobotX(), visionSystem.getRobotY());
            telemetry.addData("MoveStep: ", "%s", robot.moveStep);
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("AutonomousStep", "%s", autonomousStep.name());
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            telemetry.addData("armPosition", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.update();

        }
        visionSystem.shutdown();
    }
}
