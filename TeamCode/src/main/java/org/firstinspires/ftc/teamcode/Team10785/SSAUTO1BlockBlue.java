//better autonomous code
package org.firstinspires.ftc.teamcode.Team10785;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.DcMotor;
import java.util.List;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaSkyStone;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.robotcore.external.tfod.TfodSkyStone;

import java.util.ArrayList;

@Autonomous
public class SSAUTO1BlockBlue extends LinearOpMode {

    Robot10785 robot = new Robot10785();

    public String found;
    public int target;
    //public int autonomousStep = 1;
    public String storedMineralposition;
    private ElapsedTime runtime = new ElapsedTime();
    private double currentTime = robot.period.seconds();
    public int diagnalLength = 34;
    public int LaW = 24;
    public double startTime = 0;
    public float superFast=0.9f;
    public float maxSpeed = 0.65f;//was 0.4
    public float slowSpeed=0.4f;
    public float accTime = 1.0f;
    private float reverseDistance = 0;
    public enum AutonomousStep{
       moveFromHomePosition, strafeToFirstBlock, moveForwardToFirstBlock, lowerLift, grabBlock1, bringLiftUp1,
       moveBack1, turnToDrive1, moveUnderBar1, turnTowardsTray1, moveIntoTray1, releaseBlock1, moveAwayFromTray1,
       turnTowardsLoad1, moveBackToBlock2, turnToPickUpBlock2,moveForwardAndKnockBlock, moveToBlock2, sideWaysToLoadBlock,
        lowerLift2,grabBlock2, moveBack2, turnToTray2, moveToTray2, turnTowardsTray2, moveTowardsTray2,dropBlock2,moveToCenterTray,
        hookTray, moveToBuildZone, endAuto

    }
    public AutonomousStep autonomousStep = AutonomousStep.moveFromHomePosition;

    public final int slidemax = 461;
    public final int liftmin = -570;
    private VuforiaSkyStone vuforiaSkyStone = new VuforiaSkyStone();
    private TfodSkyStone tfodSkyStone = new TfodSkyStone();
    private final float k = -1.0f;//A constant made for ease of transferral

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);
        robot.setHomePositions();
        robot.startingAngle=0;


        telemetry.addData("Status", "Initialized 1");
        telemetry.update();


//        robot.lift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.lift.setTargetPosition(0);
        robot.lift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.lift.setPower(1);

//        robot.slide.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.slide.setTargetPosition(0);
        robot.slide.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        robot.slide.setPower(1);

        robot.tape1.setTargetPosition(0);
        robot.tape2.setTargetPosition(0);
        robot.tape1.setPower(1);
        robot.tape2.setPower(1);

        // Sample TFOD Op Mode
        // Initialize Vuforia.
        vuforiaSkyStone.initialize(
                //*Sharp inhale*
                "AbQaekX/////AAABmdZK4VbDrU0cmz7SaHl/whRyPl7Ef/qgl6dy0r02zyIydswhIKnSJslloshmr7SR0dv9mi1bXIP2WrBUXMANqvWEVuEYCXUwPF2bxWiLRuzmmfJXPzusGPfVqJlYz5DPHoh+GXzErifqDn9pND1e8pxs5hCTdAwSAG4DeyMEhRXTuuLKKusNLKyDwoGjLR7ndnRoi5iQxFKRMnkpnY6XawJvQozMzIxP9NzKe3Sgyzr7Q+yh6AJyPOHEHz1Ftx3jvNb9U+n+l2rUlJxxlAwyAKf5/ugGo/T0BDtozghrwVDi6DTgNLzlbBIC4o8ly/UpF0/rPSt+hmMt0f+OA8yDe194IMDBf2VsAXethn52oh/e",
                //AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
                hardwareMap.get(WebcamName.class, "Webcam 1"), // cameraName
                "", // webcamCalibrationFilename
                true, // useExtendedTracking
                true, // enableCameraMonitoring
                VuforiaLocalizer.Parameters.CameraMonitorFeedback.AXES, // cameraMonitorFeedback
                0, // dx
                0, // dy
                0, // dz
                0, // xAngle
                0, // yAngle
                0, // zAngle
                true); // useCompetitionFieldTargetLocations
        // Set min confidence threshold to 0.7
        tfodSkyStone.initialize(vuforiaSkyStone, 0.5F, true, true);
        // Initialize TFOD before waitForStart.
        // Init TFOD here so the object detection labels are visible
        // in the Camera Stream preview window on the Driver Station.
        tfodSkyStone.activate();


        //robot.lift2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        //robot.lift2.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        double runningavg = 0;
        boolean skystonefound = false;
        int skystonenotfoundcount = 0;
        String stoneLoc=null;
        robot.claw.setPosition(0);
        while (!opModeIsActive() && !isStopRequested()) {
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());


            telemetry.addData("status", "waiting for a start command...");
            if (tfodSkyStone != null) {
                // getUpdatedRecognitions() will return null if no new information is available since
                // the last time that call was made.
                List<Recognition> updatedRecognitions = tfodSkyStone.getUpdatedRecognitions();
                if (updatedRecognitions != null) {
                    skystonefound = false;
//                    telemetry.addData("# Object Detected", updatedRecognitions.size());
                    // step through the list of recognitions and display boundary info.
                    int i = 0;
                    for (Recognition recognition : updatedRecognitions) {
                        if (recognition.getLabel().equalsIgnoreCase("Skystone")) {
                            //telemetry.addData(String.format("label (%d)", i), recognition.getLabel());
                            runningavg = (recognition.getLeft() * 0.2) + (runningavg * 0.8);
                            skystonefound = true;
                            skystonenotfoundcount = 0;
//                            telemetry.addData(String.format("  left,top (%d)", i), "%.03f , %.03f",
//                                    recognition.getLeft(), recognition.getTop());
                        }
                    }
                }
            }
            if (!skystonefound) {
                skystonenotfoundcount++;
            }
            if (skystonenotfoundcount > 200) {//The Blue side is a little flakey, keep this at 200
                stoneLoc = "Load";//Right?
            } else if (runningavg > 300) {
                stoneLoc = "Center";//Duh
            } else {
                stoneLoc = "Build";//Left?
            }


            telemetry.addData("Skystone location: ",stoneLoc);
            telemetry.addData("  Running avg: ",  "%f",runningavg);
            telemetry.addData("Skystone not found", "%d", skystonenotfoundcount);
            telemetry.update();
        }
                //waitForStart();

        while (opModeIsActive()) {


            switch (autonomousStep) {
                case moveFromHomePosition:// LIFT LEVEL 1/Open claw/Move forward
                    robot.lift.setTargetPosition(481);
                    robot.claw.setPosition(1);
                    robot.moveHoldAngle(20f,slowSpeed,0);
                    autonomousStep = AutonomousStep.strafeToFirstBlock;
                    break;

                case strafeToFirstBlock://STRAFES TO LINE UP WITH CORRECT SKYSTONE
                    if(robot.moveComplete()) {
                        if (stoneLoc.equals("Build")) {
                            robot.strafeHoldAngle(-6.0f*k,-slowSpeed*k,0);
                        }
                        else if (stoneLoc.equals("Center")) {
                            robot.strafeHoldAngle(-1.5f*k,-slowSpeed,0);
                        }
                        else {
                            robot.strafeHoldAngle(10.5f*k,slowSpeed*k,0);
                        }
                        autonomousStep = AutonomousStep.moveForwardToFirstBlock;
                    }
                    break;
                case moveForwardToFirstBlock://MOVES FORWARD TO 1ST BLOCK
                    if(robot.moveComplete()){
                        robot.moveHoldAngle(10f,slowSpeed,0);
                        autonomousStep=AutonomousStep.lowerLift;
                    }
                    break;
                case lowerLift:
                    if (robot.moveComplete()) {
                        robot.lift.setTargetPosition(0);
                        autonomousStep = AutonomousStep.grabBlock1;
                    }
                    break;
                //}


                case grabBlock1:
                    if (Math.abs(robot.lift.getCurrentPosition()) < 10) {
                        robot.claw.setPosition(0.4);//old value 0.5
                        robot.slide.setTargetPosition(100);//max is 127
                        startTime = robot.period.seconds();
                        autonomousStep = AutonomousStep.bringLiftUp1;
                    }
                    break;


                case bringLiftUp1://BRINGS LIFT UP TO 60
                    if (robot.period.seconds() - startTime > 0.5) {
                        robot.lift.setTargetPosition(60);
                        autonomousStep=AutonomousStep.moveBack1;
                    }
                    break;

                case moveBack1: // MOVES BACKWARDS AFTER GRABBING BLOCK
                    if (Math.abs(robot.lift.getCurrentPosition()-60) < 5) {
                        robot.moveHoldAngle(-6f + reverseDistance,-maxSpeed,0); // was 12
                        autonomousStep = AutonomousStep.turnToDrive1;
                    }
                    break;

                case turnToDrive1:
                    if (robot.moveComplete()){
                        robot.turn(90f,slowSpeed);
                        autonomousStep=AutonomousStep.moveUnderBar1;
                    }
                    break;
                case moveUnderBar1:
                    if (robot.moveComplete()){
                        if (stoneLoc.equals("Build")) {
                            robot.moveHoldAngle(53.0f,superFast,90);
                        } else if (stoneLoc.equals("Center")) {
                            robot.moveHoldAngle(60.0f,superFast,90);
                        } else {
                            robot.moveHoldAngle(90.0f,superFast,90);
                        }
                        autonomousStep=AutonomousStep.turnTowardsTray1;
                    }
                    break;
                case turnTowardsTray1:
                    if (robot.moveComplete()){
                        robot.turn(0,slowSpeed);
                        robot.lift.setTargetPosition(280);
                        robot.slide.setTargetPosition(400);
                        autonomousStep=AutonomousStep.moveIntoTray1;
                    }
                    break;

                case moveIntoTray1: // LIFT UP AND MOVE SLIDE OUT AND MOVE FORWARD 10 INCHES

                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(3.5f-reverseDistance,slowSpeed,0);
                        autonomousStep = AutonomousStep.releaseBlock1;
                    }
                    break;

                case releaseBlock1:// OPEN CLAW/RELEASE BLOCK
                    if  (robot.moveComplete()) {
                        robot.claw.setPosition(1);
                        robot.lift.setTargetPosition(903);
                        startTime=robot.period.seconds();
                        autonomousStep = AutonomousStep.moveAwayFromTray1;
                    }
                    break;

                case moveAwayFromTray1: // MOVE BACK 10 INCHES
                    if (robot.period.seconds() - startTime > 0.3){
                        robot.moveHoldAngle(-3.5f + reverseDistance,-slowSpeed,0);
                        robot.slide.setTargetPosition(0);
                        robot.claw.setPosition(0);
                        autonomousStep = AutonomousStep.turnTowardsLoad1;
                    }
                    break;

                case turnTowardsLoad1:
                    if (robot.moveComplete()){
                        robot.slide.setTargetPosition(0);
                        robot.lift.setTargetPosition(0);
                        robot.turn(-90f,slowSpeed);
                        autonomousStep=AutonomousStep.moveBackToBlock2;
                    }
                    break;
                case moveBackToBlock2:
                    if (robot.moveComplete()){
                        if (stoneLoc.equals("Build")) {
                            robot.moveHoldAngle((55.0f+22.0f),superFast,-90);
                            autonomousStep=AutonomousStep.turnToPickUpBlock2;
                        } else if (stoneLoc.equals("Center")) {
                            robot.moveHoldAngle((62.0f+22.0f),superFast,-90);
                            autonomousStep=AutonomousStep.turnToPickUpBlock2;
                        } else {
                            robot.moveHoldAngle((83.0f+22.0f),superFast,-90);
                            autonomousStep=AutonomousStep.turnToPickUpBlock2;
                        }
                    }
                    break;
                case turnToPickUpBlock2:
                    if (robot.period.seconds()-startTime>3.0) {
                       //robot.claw.setPosition(1);
                        robot.slide.setTargetPosition(0);
                        if (!stoneLoc.equals("Load")) { // Don't raise lift for build need to knock and slide
                            robot.lift.setTargetPosition(481);
                        }
                    }
                    if (robot.moveComplete()){
                        robot.turn(0,slowSpeed);
                        robot.claw.setPosition(1);
                        if (stoneLoc.equals("Load")){
                            autonomousStep=AutonomousStep.moveForwardAndKnockBlock;
                        }
                        else {
                            autonomousStep=AutonomousStep.moveToBlock2;
                        }
                    }
                    break;

                case moveForwardAndKnockBlock://LOAD 2ND BLOCK
                    if (robot.period.seconds()-startTime>3.0) {
                        robot.claw.setPosition(1);
                        robot.slide.setTargetPosition(400);
                    }
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(4.0f - reverseDistance,slowSpeed,0);
                        autonomousStep = AutonomousStep.sideWaysToLoadBlock;
                    }
                    break;
                case moveToBlock2: // BUILD/CENTER MOVE TO BLOCK
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(14f - reverseDistance,slowSpeed,0);
                        autonomousStep = AutonomousStep.lowerLift2;
                    }
                    break;

                case sideWaysToLoadBlock: // STRAFE TO LOAD BLOCK
                    if (robot.moveComplete()){
                        robot.strafeHoldAngle(6.0f*k,slowSpeed*k,0);
                        autonomousStep =AutonomousStep.grabBlock2;
                    }
                    break;

                case lowerLift2: // Reset lift to ZERO
                    if (robot.moveComplete()) {
                        robot.lift.setTargetPosition(0);
                        autonomousStep =AutonomousStep.grabBlock2;
                    }
                    break;
                case grabBlock2: // CHECKS IF LIFT IS DOWN/CLAW CLOSES/SLIDE OUT
                    if (robot.lift.getCurrentPosition()<10&&robot.moveComplete()){
                        robot.claw.setPosition(0.4);//old value 0.5
                        if (!stoneLoc.equals("Load")) {
                            robot.slide.setTargetPosition(100);//max is 127
                        }
                        autonomousStep = AutonomousStep.moveBack2;
                        startTime = robot.period.seconds();
                    }
                    break;
                case moveBack2: // MOVE BACKWARDS SECOND TIME
                    if (robot.period.seconds()-startTime>0.5){
                        robot.lift.setTargetPosition(60);
                        if (stoneLoc.equals("Load")){
                            robot.moveHoldAngle( -2f + reverseDistance,-maxSpeed,0);
                        }
                        else {
                            robot.moveHoldAngle(-6f + reverseDistance,-maxSpeed,0);
                        }
                        autonomousStep=AutonomousStep.turnToTray2;
                    }
                    break;
                case turnToTray2:
                    if (robot.moveComplete()){
                        robot.turn(90f,slowSpeed);
                        autonomousStep=AutonomousStep.moveToTray2;
                    }
                    break;

                case moveToTray2:
                    if (robot.moveComplete()){
                        startTime = robot.period.seconds();
                        if (stoneLoc.equals("Build")) {
                            robot.moveHoldAngle(54.0f+24.0f+20.0f,superFast,90);
                        } else if (stoneLoc.equals("Center")) {
                            robot.moveHoldAngle(62.0f+24.0f+20.0f,superFast,90);
                        } else {
                            robot.moveHoldAngle(50.0f+24.0f+20.0f,superFast,90);
                        }
                        autonomousStep=AutonomousStep.turnTowardsTray2;
                    }
                    break;
                case turnTowardsTray2:
                    if (stoneLoc.equals("Load") && robot.period.seconds() - startTime > 1.75){
                        robot.lift.setTargetPosition(480);
                    }
                    if (robot.moveComplete()){
                        robot.turn(0,slowSpeed);
                        if (stoneLoc.equals("Load")){
                            robot.lift.setTargetPosition(480);
                        }
                        else{
                            robot.lift.setTargetPosition(280);
                        }
                        robot.slide.setTargetPosition(400);
                        autonomousStep=AutonomousStep.moveTowardsTray2;
                    }
                    break;

                case moveTowardsTray2: // LIFT UP AND MOVE SLIDE OUT AND MOVE FORWARD 12 INCHES
                    if (robot.moveComplete()) {
                        robot.moveHoldAngle(5.0f-reverseDistance,slowSpeed,0);
                        autonomousStep = AutonomousStep.dropBlock2;
                    }
                    break;

                case dropBlock2://CLAW OPEN
                    if (robot.moveComplete()) {
                        robot.claw.setPosition(1);
                        robot.lift.setTargetPosition(803);
                        startTime=robot.period.seconds();
                        autonomousStep=AutonomousStep.moveToCenterTray;
                    }
                    break;
                case moveToCenterTray://CLOSE CLAW
                    if (robot.period.seconds()-startTime>0.3){
                        if (stoneLoc.equals("Load")){
                            robot.strafeHoldAngle(5.0f, maxSpeed, 0);
                        }
                        else {
                            robot.strafeHoldAngle(-10.0f, -maxSpeed, 0);
                        }
                        robot.claw.setPosition(0);
                       // robot.slide.setTargetPosition(250);
                        autonomousStep=AutonomousStep.hookTray;
                    }
                    break;
                case hookTray: //MOVE FORWARD 4 INCHES AND SET LIFT POSITION TO 0 AND EXTEND TAPE AND MOVE SLIDE IN
                    if(robot.moveComplete()){
                        robot.moveHoldAngle(4.0f,slowSpeed,0);
                        robot.lift.setTargetPosition(-30);
                        robot.tape2.setTargetPosition((int)(70*robot.countsPerInchTape));
                        autonomousStep = AutonomousStep.moveToBuildZone;
                    }
                    break;

                case moveToBuildZone: //MOVE BACK
                    if (robot.lift.getCurrentPosition()<-10 && robot.moveComplete()){
                        robot.moveHoldAngle(-40,-maxSpeed,0);
                        robot.slide.setTargetPosition(140);
                        autonomousStep = AutonomousStep.endAuto;
                    }
                    break;

            }

            robot.moveUpdate();
            telemetry.addData("Status", "Running");
            telemetry.addData("MoveStep", "%d", robot.moveStep);
            telemetry.addData("Found", "%s", found);
            telemetry.addData("StoredFound", "%s", storedMineralposition);
            //telemetry.addData("Pos (in)", "{X, Y, Z} = %.1f, %.1f, %.1f",
            telemetry.addData("armPosition", "%d ", robot.lift.getCurrentPosition());
            telemetry.addData("robot motor position", "%d", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("AutonomousStep", "%s", autonomousStep.name());
            telemetry.addData("Lift Position", "%d", robot.lift.getCurrentPosition());
            telemetry.addData("Robot Angle", "%.1f", robot.getRobotAngle());
            telemetry.update();

        }
    }
}
